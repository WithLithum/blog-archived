---
layout: post
title:  "如何加入RelaperStack房间版"
date:   2021-09-03 12:58:37 +0800
categories: 多人游戏
---

本房间即日起*部分*迁出网易我的世界中国版**本地联机**区。

## 加入方式

在加入前，您需要知晓：

* 您可能需要第三方工具，如nat123、PCL等来连接 RelaperStack。

* 我们不接受 QQ 和其它类似平台作为联系方式。

* 您需要 MpConnect 连接我们的房间。

### 下载并安装 RelaperMP Connect

* 前往[项目发布页面](https://gitlab.com/WithLithum/MpConnect/-/releases)。

* 找到最新版本，点击 Software package（软件包）中的**安装包**。

* 下载并安装 RelaperMP Connect。

* 进入 RelaperMP Connect 页面。如果当前我们有房间开放，我们会通过此客户端显示。

此软件目前不会主动通知您，也不能运行在后台。

## IRC

我们的 IRC 频道为 `#relapermp`，位于 `EsperNet` IRC 网络。请注意该 IRC 网络禁止诸如联通等违反此网络条例的运营商连接。若您使用此类网络，您将无法参与聊天，但您仍然可以游玩 RelaperStack。

我们推荐 HexChat 作为您的 IRC 客户端——此客户端开源、免费。注意如果您在 UWP 平台上发现该应用需要收费，请下载试用版。付费版和试用版是一致的，购买付费版仅仅是支持作者而已。
