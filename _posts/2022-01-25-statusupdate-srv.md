---
layout: post
title: RelaperStack / GC 状态更新（2022年1月）
date: 2022-01-25 08:07:19 +0800
categories: 服务器
---

所以估计很久都没发什么东西了，今天发一篇吧。

## 功能

目前下列来自 RelaperStack 的命令已实现：

- `/blip`
- `/spawn`

另有下列新增命令：

- `/tps`

其它命令可能被其它插件取代，但此列表只列出被核心插件取代的命令。

另外，这些是被原版/其它插件取代的命令：

- `/trigger block`（被 `/ban` 取代）
- `/trigger jail`（被 Essentials `/jail` 取代）
- `/trigger kick`（被 `/kick` 取代）
- `/trigger op`（被 LuckPerms 和 `/op` 取代）

### 游戏玩法

更新添加了一些原来没有的功能，比如击杀生物时的提示音和快捷栏消息提示：

![图 1](/assets/2022_01_kill_notify.gif)

命中标靶方块时的提示音和信号等级提示：

![图 2](/assets/2022_01_target_notify.gif)

### 安全内容

在之前的 RelaperStack 中，你无法引爆 TNT 来破坏地形 —— 它会被一个自动生成的苦力怕取代，而这个苦力怕是立即爆炸的（因为命令是无法直接造成爆炸的）。

在新版插件中，试图使用 TNT 来破坏地形会在其爆炸的一瞬间[被拦截](https://gitlab.com/BudPlaza/budplaza-software-paper/-/blob/main/src/main/java/nws/lithiumdev/budplaza/software/mod/events/handlers/EntityEventHandlers.java#L78)，同时会[创建一个新的](https://gitlab.com/BudPlaza/budplaza-software-paper/-/blob/main/src/main/java/nws/lithiumdev/budplaza/software/mod/events/handlers/EntityEventHandlers.java#L77)、不破坏地形的爆炸。目前还未实现继承爆炸制造者实体的功能。

下图演示了这个功能：

![图 3](/assets/2022_01_tnt.gif)

顺带一提，目前版本还没有拦截方块爆炸的功能。

## 其它插件

目前测试用的服务端里面还有这些插件：

- Geyser (Floodgate)
- EssentialsX
- CoreProtect
- LuckPerms
- WorldEdit
- WorldGuard
- CensorReloaded
- ClearLag

估计以后都得放进正式端，尤其是 CoreProtect，毕竟这玩意能回滚玩家操作。

CensorReloaded 说实话可能真的没什么用处，毕竟关键词屏蔽，稍有不慎会令沟通非常困难。

## 结束语

这个项目开发的时间已经有点久了。我希望今年能完成这个项目，如果你有时间的话，可以去[看看](https://gitlab.com/budplaza/budplaza-software-paper)这个项目，或者贡献点什么玩意，以及开一些有意思的议题。

至于网易？我已经不想在上面花时间了。总之最后我还会宽限一段时间的 `online-mode=false`，然后看看能不能移动到 BDS。
